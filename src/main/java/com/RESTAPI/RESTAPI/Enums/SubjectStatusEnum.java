package com.RESTAPI.RESTAPI.Enums;

public enum SubjectStatusEnum {
    PENDING,
    ACTIVE,
    CLOSED
}
