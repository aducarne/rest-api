package com.RESTAPI.RESTAPI.Enums;

public enum GenderEnum {
    MALE,
    FEMALE,
    NON_BINARY
}
