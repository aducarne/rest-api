package com.RESTAPI.RESTAPI.Entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Proposition {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @ManyToOne
    private Subject subject;

    private int scoreFor;
    private int scoreYesBut;
    private int scoreNeutral;
    private int scoreAgainst;
}
