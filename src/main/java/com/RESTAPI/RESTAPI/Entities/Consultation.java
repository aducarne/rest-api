package com.RESTAPI.RESTAPI.Entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Consultation {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private LocalDateTime reviewDate;
    @ManyToOne
    private Subject subject;
    @ManyToOne
    private User user;
}
