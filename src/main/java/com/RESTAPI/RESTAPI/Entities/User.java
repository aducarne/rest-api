package com.RESTAPI.RESTAPI.Entities;


import com.RESTAPI.RESTAPI.Enums.GenderEnum;
import com.RESTAPI.RESTAPI.Enums.UserTypeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String type;
    private LocalDate birthDate;

    @Column(length = 12)
    private String phone;

    @Enumerated(EnumType.STRING)
    private UserTypeEnum userTypeEnum;

    @Enumerated(EnumType.STRING)
    private GenderEnum sex;

    @OneToOne
    private Account account;
}