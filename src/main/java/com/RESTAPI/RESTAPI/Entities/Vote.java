package com.RESTAPI.RESTAPI.Entities;

import com.RESTAPI.RESTAPI.Enums.ChoiceEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Vote {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    Consultation consultation;

    @ManyToOne
    Proposition proposition;

    @Enumerated(EnumType.STRING)
    ChoiceEnum choice;

    @ManyToOne
    User user;

    String comment;
}
