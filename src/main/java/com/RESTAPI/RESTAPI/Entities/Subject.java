package com.RESTAPI.RESTAPI.Entities;

import com.RESTAPI.RESTAPI.Enums.SubjectStatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Subject {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private LocalDate endDate;

    @Enumerated(EnumType.STRING)
    private SubjectStatusEnum status;
}
