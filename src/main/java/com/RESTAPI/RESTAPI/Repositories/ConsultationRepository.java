package com.RESTAPI.RESTAPI.Repositories;

import com.RESTAPI.RESTAPI.Entities.Consultation;
import org.springframework.data.repository.CrudRepository;

public interface ConsultationRepository extends CrudRepository<Consultation, Integer> {

}
