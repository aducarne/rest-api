package com.RESTAPI.RESTAPI.Repositories;

import com.RESTAPI.RESTAPI.Entities.Subject;
import org.springframework.data.repository.CrudRepository;

public interface SubjectRepository extends CrudRepository<Subject, Integer> {

}
