package com.RESTAPI.RESTAPI.Repositories;

import com.RESTAPI.RESTAPI.Entities.Proposition;
import org.springframework.data.repository.CrudRepository;

public interface PropositionRepository extends CrudRepository<Proposition, Integer> {

}
