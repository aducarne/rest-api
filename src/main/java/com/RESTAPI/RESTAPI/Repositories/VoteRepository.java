package com.RESTAPI.RESTAPI.Repositories;

import com.RESTAPI.RESTAPI.Entities.Vote;
import org.springframework.data.repository.CrudRepository;

public interface VoteRepository extends CrudRepository<Vote, Integer> {

}
