package com.RESTAPI.RESTAPI.Repositories;

import com.RESTAPI.RESTAPI.Entities.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Integer> {

}
