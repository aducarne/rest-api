package com.RESTAPI.RESTAPI.Controllers;

import com.RESTAPI.RESTAPI.Entities.Account;
import com.RESTAPI.RESTAPI.Repositories.AccountRepository;
import com.RESTAPI.RESTAPI.Requests.AccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
@RequestMapping(path = "/account")
public class AccountController {
    @Autowired
    private AccountRepository accountRepository;

    @PutMapping
    public ResponseEntity<Account> create(@RequestBody AccountRequest accountRequest) {
        Account account = Account.builder()
                .email(accountRequest.getEmail())
                .pwd(accountRequest.getPwd())
                .isActive(accountRequest.getIsActive())
                .creationDate(LocalDate.now())
                .build();
        return new ResponseEntity<>(accountRepository.save(account), HttpStatus.OK);
    }


    @GetMapping(path = "/{id}")
    public @ResponseBody
    ResponseEntity<Account> getById(@PathVariable Integer id) {
        return accountRepository
                .findById(id)
                .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public @ResponseBody
    Iterable<Account> getAll() {
        return accountRepository.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Integer> delete(@PathVariable int id) {
        try {
            accountRepository.deleteById(id);
        }
        catch (Exception e) {
            return new ResponseEntity<>(id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}