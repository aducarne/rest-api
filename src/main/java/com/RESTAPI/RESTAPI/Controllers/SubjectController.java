package com.RESTAPI.RESTAPI.Controllers;

import com.RESTAPI.RESTAPI.Entities.Subject;
import com.RESTAPI.RESTAPI.Repositories.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/subject")
public class SubjectController {
    @Autowired
    private SubjectRepository subjectRepository;

    @PutMapping
    public @ResponseBody
    Subject create(@RequestBody Subject subject) {
        return subjectRepository.save(subject);
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    ResponseEntity<Subject> getById(@PathVariable int id) {
        return subjectRepository.findById(id)
                .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public @ResponseBody
    Iterable<Subject> getAll() {
        return subjectRepository.findAll();
    }
}