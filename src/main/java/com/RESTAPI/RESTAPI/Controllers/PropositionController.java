package com.RESTAPI.RESTAPI.Controllers;

import com.RESTAPI.RESTAPI.Entities.Proposition;
import com.RESTAPI.RESTAPI.Repositories.PropositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Controller
@RequestMapping(path="/proposition")
public class PropositionController {
    @Autowired
    private PropositionRepository propositionRepository;

    @PutMapping
    public @ResponseBody
    ResponseEntity<Proposition> create(@RequestBody Proposition proposition)
    {
        return Objects.nonNull(proposition.getSubject()) ?
                new ResponseEntity<>(propositionRepository.save(proposition), HttpStatus.OK)
                : new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    public @ResponseBody
    Iterable<Proposition> getAll()
    {
        return propositionRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    ResponseEntity<Proposition> getById(@PathVariable int id)
    {
        return propositionRepository.findById(id)
                .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}