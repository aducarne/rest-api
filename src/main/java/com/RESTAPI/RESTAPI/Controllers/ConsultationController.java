package com.RESTAPI.RESTAPI.Controllers;

import com.RESTAPI.RESTAPI.Entities.Consultation;
import com.RESTAPI.RESTAPI.Repositories.ConsultationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Objects;

@Controller
@RequestMapping(path="/consultation")
public class ConsultationController {
    @Autowired
    private ConsultationRepository consultationRepository;

    @PutMapping
    public @ResponseBody
    Consultation create(@RequestBody Consultation consultation)
    {
        consultation.setReviewDate(LocalDateTime.now());

        return Objects.nonNull(consultation.getSubject()) ?
                consultationRepository.save(consultation)
                : new Consultation();
    }

    @GetMapping
    public @ResponseBody
    Iterable<Consultation> getAll()
    {
        return consultationRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    ResponseEntity<Consultation> getById(@PathVariable Integer id) {
        return consultationRepository.findById(id)
                .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}