package com.RESTAPI.RESTAPI.Controllers;

import com.RESTAPI.RESTAPI.Entities.Vote;
import com.RESTAPI.RESTAPI.Enums.ChoiceEnum;
import com.RESTAPI.RESTAPI.Repositories.PropositionRepository;
import com.RESTAPI.RESTAPI.Repositories.VoteRepository;
import javassist.tools.web.BadHttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/vote")
public class VoteController {
    @Autowired
    private PropositionRepository propositionRepository;
    @Autowired
    private VoteRepository voteRepository;

    @PutMapping
    public ResponseEntity<Vote> create(@RequestBody Vote vote) {
        try {
            handleChoice(vote.getChoice(), vote.getProposition().getId());
        } catch (BadHttpRequest e) {
            return new ResponseEntity<>(new Vote(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(voteRepository.save(vote), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    ResponseEntity<Vote> getById(@PathVariable Integer id) {
        return voteRepository.findById(id)
                .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public @ResponseBody
    Iterable<Vote> getAll() {
        return voteRepository.findAll();
    }

    private void handleChoice(ChoiceEnum choice, int id) throws BadHttpRequest {
        propositionRepository.findById(id)
                .map(p -> {
                    switch (choice) {
                        case FOR:
                            p.setScoreFor(p.getScoreFor() + 1);
                            break;
                        case FOR_BUT:
                            p.setScoreYesBut(p.getScoreYesBut() + 1);
                            break;
                        case NEUTRAL:
                            p.setScoreNeutral(p.getScoreNeutral() + 1);
                            break;
                        case AGAINST:
                            p.setScoreAgainst(p.getScoreAgainst() + 1);
                            break;
                    }
                    return propositionRepository.save(p);
                })
                .orElseThrow(BadHttpRequest::new);
    }
}