package com.RESTAPI.RESTAPI.Controllers;

import com.RESTAPI.RESTAPI.Entities.User;
import com.RESTAPI.RESTAPI.Repositories.AccountRepository;
import com.RESTAPI.RESTAPI.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
@RequestMapping(path="/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;

    @PutMapping
    public @ResponseBody
    User add(@RequestBody User user) {
        user.getAccount().setCreationDate(LocalDate.now());
        accountRepository.save(user.getAccount());
        return userRepository.save(user);
    }


    @GetMapping(path = "/{id}")
    public @ResponseBody
    ResponseEntity<User> getById(@PathVariable Integer id) {
        return userRepository.findById(id)
                .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public @ResponseBody
    Iterable<User> getAll() {
        return userRepository.findAll();
    }

    @DeleteMapping(path="/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        try {
            userRepository.deleteById(id);
        }
        catch (Exception e) {
            return new ResponseEntity<>(id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity<User> update(@RequestBody User user) {
        return new ResponseEntity<>(userRepository.save(user), HttpStatus.OK);
    }
}