package com.RESTAPI.RESTAPI.Requests;

import lombok.Getter;

@Getter
public class AccountRequest {
    private String email;
    private String pwd;
    private Boolean isActive;
}
