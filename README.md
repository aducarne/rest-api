# REST-API
Simple REST API with Sprint Boot, Hibernate for MySQL.

## Requirements
*  Lombok plugin for your IDE

*  Java 11 JDK


## Installation
Download and Install Lombok plugin for your IDE
Make sure you enable Annotation Processors in your IDE.

## Usage
run task bootrun from Gradle (./gradlew bootrun)
edit application.properties to your liking.

[Postman Collection](https://www.getpostman.com/collections/a2b064f2daaa5894e3d3)

## Troubleshooting
if you don't want to use Lombok, remove annotations and replace them with the corresponding code or use Delombok.